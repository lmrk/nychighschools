package com.example.a20230316_vanessacruz_nycschools.ui.details

import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolSATResults

sealed class DetailsState {
    object None : DetailsState()
    object Loading : DetailsState()
    object SATFailure : DetailsState()
    object Failure : DetailsState()
    data class SATSuccess(val satData: HighSchoolSATResults) : DetailsState()

    data class HighSchoolSuccess(val highSchool: HighSchool) : DetailsState()
}