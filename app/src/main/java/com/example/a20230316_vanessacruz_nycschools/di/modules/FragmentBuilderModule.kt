package com.example.a20230316_vanessacruz_nycschools.di.modules

import com.example.a20230316_vanessacruz_nycschools.ui.details.DetailsFragment
import com.example.a20230316_vanessacruz_nycschools.ui.schools.SchoolsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun provideHighSchoolListingFragment(): SchoolsFragment

    @ContributesAndroidInjector
    abstract fun provideHighSchoolDetailsFragment(): DetailsFragment

}
