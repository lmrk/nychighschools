package com.example.a20230316_vanessacruz_nycschools.api

import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolSATResults
import retrofit2.http.GET
import retrofit2.http.Query

interface SATApi {

    @GET("f9bf-2cp4.json")
    suspend fun fetchSATScore(@Query("dbn") dbn: String): List<HighSchoolSATResults>
}
