package com.example.a20230316_vanessacruz_nycschools

import com.example.a20230316_vanessacruz_nycschools.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber
import timber.log.Timber.DebugTree

class NYCHighSchoolsApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            if (BuildConfig.DEBUG) {
                Timber.plant(DebugTree())
            }
        }
    }
}