package com.example.a20230316_vanessacruz_nycschools.repository

import com.example.a20230316_vanessacruz_nycschools.api.SATApi
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolSATResults
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface SATRepositoryContract {

    suspend fun fetchSATResults(dbn: String): HighSchoolSATResults?
    var selectedSchool: HighSchool?
}

class SATRepository(
    private val schoolScoresApi: SATApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
    override var selectedSchool: HighSchool? = null,
) : SATRepositoryContract {

    val schools = mutableListOf<HighSchool>()
    val scores = mutableListOf<HighSchoolSATResults>()

    override suspend fun fetchSATResults(dbn: String): HighSchoolSATResults? =
        withContext(dispatcher) {
            val localScore = fetchScoreFromCache(dbn)
            if (localScore != null) return@withContext localScore
            val scoresFromApi = schoolScoresApi.fetchSATScore(dbn)
            synchronized(this) {
                this@SATRepository.scores.apply {
                    clear()
                    addAll(scoresFromApi)
                }
            }
            return@withContext fetchScoreFromCache(dbn)
        }

    private fun fetchScoreFromCache(dbn: String) = scores.firstOrNull { it.dbn == dbn }
}