package com.example.a20230316_vanessacruz_nycschools.viewmodel

import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolAndSAT
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolSATResults
import com.example.a20230316_vanessacruz_nycschools.model.NetworkResponse
import com.example.a20230316_vanessacruz_nycschools.repository.SATRepository
import com.example.a20230316_vanessacruz_nycschools.ui.details.DetailsViewModel
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailsViewModelTest {

    @Mock
    private lateinit var satRepository: SATRepository

    private lateinit var detailsViewModel: DetailsViewModel

    @Before
    fun setUp() {
        detailsViewModel = DetailsViewModel(satRepository)
    }

    @Test
    fun `test fetchSchoolsAndScores with valid school`() = runBlocking {
        //Given
        val school = HighSchool("123443", "testSchool")
        val score = HighSchoolSATResults("123443",
            "TestSchoolName",
            "234",
            "897",
            "098",
            "0990")
        Mockito.`when`(satRepository.selectedSchool).thenReturn(school)
        Mockito.`when`(satRepository.fetchSATResults(school.dbn!!)).thenReturn(score)

        //When
        detailsViewModel.fetchSchoolAndScores()

        //Then
        val expectedValue = NetworkResponse.Success(HighSchoolAndSAT(school, score))
        assertEquals(expectedValue, detailsViewModel.schoolData)
    }
}