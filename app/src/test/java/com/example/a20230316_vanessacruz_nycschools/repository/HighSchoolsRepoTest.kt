package com.example.a20230316_vanessacruz_nycschools.repository

import com.example.a20230316_vanessacruz_nycschools.api.SchoolsApi
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.model.NetworkResponse.Success
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class HighSchoolsRepoTest {
    lateinit var repository: HighSchoolsRepository

    @MockK
    lateinit var highSchoolApi: SchoolsApi

    @MockK
    lateinit var fakeHighSchoolResponse: List<HighSchool>

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repository = HighSchoolsRepository(highSchoolApi)
    }

    @Test(expected = java.lang.Exception::class)
    fun `on api failure, fetching NYCHighSchools returns a failure`(): Unit = runBlocking {
        //Given
        coEvery { highSchoolApi.fetchHighSchools() }.throws(java.lang.Exception())

        //when
        repository.fetchHighSchoolsList()
    }

    @Test
    fun `on api Success, fetching NYCHighSchools returns a Success with the data`() = runBlocking {
        //Given
        coEvery { highSchoolApi.fetchHighSchools() }.returns(fakeHighSchoolResponse)

        //when
        val response = repository.fetchHighSchoolsList()

        //then
        assertEquals(fakeHighSchoolResponse, response)
    }
}